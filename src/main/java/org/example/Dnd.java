package org.example;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Dnd implements Initializable {

    @FXML
    private ImageView ironman;

    @FXML
    private ImageView hulk;

    ImageView imageView1;

    @FXML
    private ImageView thor;


    @FXML
    private HBox hbThor;

    @FXML
    private ImageView thorPoner;

    @FXML
    private HBox hbIroman;

    @FXML
    private HBox hbHulk;

    @FXML
    private HBox hbSpiderman;

    @FXML
    private HBox check1;

    @FXML
    private HBox check2;

    @FXML
    private HBox check3;

    @FXML
    private HBox check4;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/ironman.png");
        Image image = new Image(file.toURI().toString());
        ironman.setImage(image);
        habilitar(ironman);
        habilitar(hulk);
//        habilitar(thor);
//        habilitar(spiderman);
        setupGestureTarget(hbIroman);
        setupGestureTarget(hbHulk);
    }

    public void habilitar(ImageView source){
        source.setOnDragDetected(mouseEvent -> {
            Dragboard dragboard = source.startDragAndDrop(TransferMode.MOVE);

            //posem la imatge en el clipboard
            ClipboardContent content = new ClipboardContent();
            Image sourceImage = source.getImage();
            content.putImage(sourceImage);
            dragboard.setContent(content);

            imageView1 = source;

            mouseEvent.consume();

        });

        source.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                source.setCursor(Cursor.HAND);
            }
        });


        source.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                source.setCursor(Cursor.HAND);
            }
        });
    }
    private void setupGestureTarget(HBox hb) {

        hb.setOnDragOver(event -> {
            Dragboard dragboard = event.getDragboard();
            System.out.println("ENtraa");
            if(dragboard.hasImage()){

                event.acceptTransferModes(TransferMode.MOVE);
            }
            event.consume();
        });

        hb.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard dragboard = event.getDragboard();

                if(dragboard.hasImage()){
                    imageView1.setImage(dragboard.getImage());
                    hb.getChildren().add(imageView1);
                    event.setDropCompleted(true);
                }else{
                    event.setDropCompleted(false);
                }
                event.consume();
            }
        });
    }

}

